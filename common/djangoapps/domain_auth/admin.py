from django.contrib import admin

from .models import Domain

class DomainAdmin(admin.ModelAdmin):
    list_display = ['domain', 'auth_code', 'auth_type', 'auth_name', 'valid_from', 'valid_to', 'is_active']
    list_filter = ['is_active']
    search_fields = ['domain']

admin.site.register(Domain, DomainAdmin)

