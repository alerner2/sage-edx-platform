"""
WE'RE USING MIGRATIONS!
If you make changes to this model, be sure to create an appropriate migration
file and check it in at the same time as your model changes. To do that,
1. Go to the edx-platform dir
2. ./manage.py lms schemamigration domain_auth --auto description_of_your_change
3. Add the migration file created in edx-platform/common/djangoapps/domain_auth/migrations/
"""

from django.db import models
from datetime import datetime

class Domain(models.Model):
    domain = models.CharField(max_length=255, db_index=True)
    valid_from = models.DateField(blank=False)
    valid_to = models.DateField(blank=False)
    auth_code = models.CharField(max_length=60, db_index=True)
    auth_type = models.CharField(max_length=60, blank=True, db_index=True)
    auth_name = models.CharField(max_length=255, blank=True, db_index=True)
    is_active = models.BooleanField(default=True)
    dtcreated = models.DateTimeField('creation date', auto_now_add=True)

    def __unicode__(self):
        return u'%s %s %s %s' % (self.domain, self.valid_from, self.valid_to, self.is_active)  
   

